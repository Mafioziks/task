-- MySQL dump 10.13  Distrib 5.5.54, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: task
-- ------------------------------------------------------
-- Server version	5.5.54-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `answer`
--

DROP TABLE IF EXISTS `answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answer` (
  `answer_id` int(11) NOT NULL AUTO_INCREMENT,
  `answer` text,
  `question_id` int(11) DEFAULT NULL,
  `answer_no` int(11) DEFAULT NULL,
  PRIMARY KEY (`answer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answer`
--

LOCK TABLES `answer` WRITE;
/*!40000 ALTER TABLE `answer` DISABLE KEYS */;
INSERT INTO `answer` VALUES (1,'Mazu',1,1),(2,'Normālu',1,2),(3,'Lielu',1,3),(4,'Milzīgu',1,4),(5,'Kafejnīca',2,1),(6,'Mājas',2,2),(7,'Ceļš',2,3),(8,'Jebkura, kur pieejama kafija',2,4),(9,'Tuvākajā vietā pie mājām/darba',3,1),(10,'Narvesenā/Statoilā',3,2),(11,'Lielveikalā',3,3),(12,'Nav konkrētas vietas',3,4),(13,'Aromāts',4,1),(14,'Garša',4,2),(15,'Pēcgarša',4,3),(16,'Kofeīns',4,4),(17,'Esmu miegaināks',5,1),(18,'Plānoju dzert kafiju',5,2),(19,'Vienkārši aizmirstu par to',5,3),(20,'Ir sajūta, ka kaut kas trūkst',5,4),(21,'Arabika',6,1),(22,'Robusta',6,2),(23,'Arabika un Robusta pupiņu maisījumam',6,3),(24,'Vienalga',6,4),(25,'Labsajūtu',7,1),(26,'Enerģiju/ uzmundrinājumu',7,2),(27,'Mieru',7,3),(28,'Svētku sajūtu',7,4),(29,'Teiktu, „Super, paldies!”',8,1),(30,'Teiktu, „Nē, paldies!”',8,2),(31,'Paskatītos uz viņu kā uz dīvaini',8,3),(32,'Aplietu viņu ar šo kafiju',8,4),(33,'Gards uzmundrinājums',9,1),(34,'Labs dienas sākums',9,2),(35,'Neatņemama ikdienas sastāvdaļa',9,3),(36,'Lieliska svētku reizēm',9,4),(37,'Tā var beigties',10,1),(38,'Tā ir nemākulīgi pagatavota',10,2),(39,'Tā ir rūgta',10,3),(40,'Tā ir vāja',10,4),(41,'Jā',11,1),(42,'Pat vairākas',11,2),(43,'Nav',11,3),(44,'Nav, bet vēlētos tādu iegādāties',11,4),(45,'Melnu kafiju',12,1),(46,'Melnu kafiju ar cukuru un pienu',12,2),(47,'Espresso',12,3),(48,'Kapučīno',12,4),(49,'Latte',12,5),(50,'Frapučino',12,6),(51,'Bezkofeīna',12,7),(52,'Šķīstošā kafija',12,8),(53,'Pirms pāris minūtēm',13,1),(54,'Dzeru tieši šobrīd',13,2),(55,'Pirms vairākām stundām',13,3),(56,'Tas bija diezgan sen',13,4);
/*!40000 ALTER TABLE `answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question` (
  `question_id` int(11) NOT NULL AUTO_INCREMENT,
  `question` text,
  `question_no` int(11) NOT NULL,
  `questionnaire_id` int(11) NOT NULL,
  PRIMARY KEY (`question_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question`
--

LOCK TABLES `question` WRITE;
/*!40000 ALTER TABLE `question` DISABLE KEYS */;
INSERT INTO `question` VALUES (1,'Kāda izmēra krūzīti Tu parasti izvēlies, ja pērc kafiju līdzņemšanai?',1,1),(2,'Kas ir Tava iecienītākā vieta kafijas baudīšanai?',2,1),(3,'Kur Tu parasti iegādājies kafiju?',3,1),(4,'Kas Tev visvairāk patīk kafijā?',4,1),(5,'Pabeidz teikumu: „ Kad es nedzeru kafiju, es...”',5,1),(6,'Kāda veida kafijas pupiņām Tu dod priekšroku?',6,1),(7,'Pabeidz teikumu: „Kafija man dod...”',7,1),(8,'Ļoti izskatīgs puisis/ meitene piedāvā Tev savu kafiju. Kā Tu reaģētu?',8,1),(9,'Kurš raksturojums, Tavuprāt, ir tieši par kafiju?',9,1),(10,'Kas ir sliktākais attiecībā uz kafiju?',10,1),(11,'Vai tev ir pašam sava termokrūze, kuru lieto kafijai?',11,1),(12,'Kādu kafiju parasti Tu parasti vislabprātāk izvēlies?',12,1),(13,'Pirms cik ilga laika Tu pēdējo reizi dzēri kafiju?',13,1);
/*!40000 ALTER TABLE `question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questionnaire`
--

DROP TABLE IF EXISTS `questionnaire`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionnaire` (
  `questionnaire_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text,
  PRIMARY KEY (`questionnaire_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questionnaire`
--

LOCK TABLES `questionnaire` WRITE;
/*!40000 ALTER TABLE `questionnaire` DISABLE KEYS */;
INSERT INTO `questionnaire` VALUES (1,'Pie kāda kafijas baudītāja tipa tu piederi?');
/*!40000 ALTER TABLE `questionnaire` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_answer`
--

DROP TABLE IF EXISTS `user_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_answer` (
  `user_id` int(11) NOT NULL,
  `questionnaire_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer_id` int(11) NOT NULL,
  UNIQUE KEY `user_id` (`user_id`,`questionnaire_id`,`question_id`,`answer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_answer`
--

LOCK TABLES `user_answer` WRITE;
/*!40000 ALTER TABLE `user_answer` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_answer` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-03-23  0:36:40
