$('.btn-toggle').click(function (event) {
    var choiced = $('.btn-toggle-selected');

    if (choiced.length > 0) {
        $(choiced).children('input').prop('checked', false);
        $(choiced).removeClass('btn-toggle-selected');
    }

    $(this).children('input').prop('checked', true);
    $(this).addClass('btn-toggle-selected');
});