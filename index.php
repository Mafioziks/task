<?php

include('vendor/autoload.php');

use Controller\QuestionnaireController;
use Model\Questionnaire;
use Model\User;
use Model\UserAnswer;

session_start();

// Show questionnare list
$quests = new QuestionnaireController();

// Reset user if needed
if (!empty($_GET['reset'])) {
    if (isset($_SESSION['uid'])) {
        unset($_SESSION['uid']);
    }
}

// If user add form is submitted - add user
if (empty($_SESSION['uid']) && !empty($_POST['uname'])) {
    $quests->addUser();
    exit;
}

// Show login if user not logged in
if (empty($_SESSION['uid'])) {
    $quests->showLogin();
    exit;
}

// Show lis of questionnaires
if (empty($_GET['questionnaire_id'])) {
    $quests->showQuestionaireList();
    exit;
}

// Save answer if got
if (!empty($_POST['answer'])) {
    (new UserAnswer())->setUserId($_SESSION['uid'])
        ->setQuestionnaireId($_GET['questionnaire_id'])
        ->setQuestionId($_GET['question_id'] - 1)
        ->setAnswerId($_POST['answer'])
        ->save();
}

// Show question
$quests->showQuestion(
    $_GET['questionnaire_id'], 
    empty($_GET['question_id']) ? 1 : $_GET['question_id']
);