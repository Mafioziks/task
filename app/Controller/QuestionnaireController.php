<?php

namespace Controller;

use Model\Questionnaire;
use Model\User;
use Model\UserAnswer;

class QuestionnaireController {

    /**
     * Show list of questionnaires in database
     */
    public function showQuestionaireList() {
        $quest = new Questionnaire();
        $questionnaires = $quest->getAllQuestionnaires();

        include(__DIR__ . '/../View/QuestionnaireList.php');
    }

    /**
     * Add user to database and create session for that user
     */
    public function addUser() {
        $user = new User();
        $user->setName($_POST['uname']);
        $user->save();
        $_SESSION['uid'] = $user->getId();
        header('Location: /');
    }

    /**
     * Show login for user before start test filling
     */
    public function showLogin() {
        include(__DIR__ . '/../View/Login.php');
    }

    /**
     * Show question with answer choices
     */
    public function showQuestion($questionnaire, $questionNo) {
        $quest = Questionnaire::getById($questionnaire);
        if ($quest == null) {
            header('Location: /');
        }
        $questions = $quest->getQuestions();

        if (empty($questions)) {
            header('Location: /');
        }

        $currentQuestion = null;
        foreach ($questions as $question) {
            if ($question->getQuestionNo() == $questionNo) {
                $currentQuestion = $question;
                break;
            }
        }

        if (empty($currentQuestion)) {
            if (!is_int($questionNo)) {
                header('Location: /');
            }

            include(__DIR__ . '/../View/Thanks.php');
            exit;
        }

        $answers = $currentQuestion->getAnswers();
        if (empty($answers)) {
            header('Location: /');
        }

        include(__DIR__ . '/../View/Question.php');
    }
}