<?php

namespace Controller;

use Model\User;
use Model\Question;
use Model\Questionnaire;
use Model\UserAnswer;

class TestCheckerController {

    /**
     * Check if user has done test
     *
     * @param int
     * @param int
     * @return boolean
     */
    public function isTestFilledByUser($testId, $userId) {
        $userManager = new User();
        $user = $userManager->getById($userId);
        if (empty($user)) {
            return false;
        }
        return in_array($testId, $user->getFilledTestIds());
    }

    /**
     * Get answers for an question
     *
     * @param int
     * @param int
     * @return array
     */
    public function getQuestionAswerArray($testId, $questionId) {
        $questionnaire = Questionnaire::getById($testId);
        $questions = $questionnaire->getQuestions();
        if ($questions == null) {
            return array();
        }

        $currentQestion = null;
        foreach ($questions as $question) {
            if ($question->getQuestionNo() == $questionId) {
                $currentQestion = $question;
                break;
            }
        }

        if ($currentQestion == null) {
            return array();
        }

        $answers = $currentQestion->getAnswers();

        if ($answers == null) {
            return array();
        }

        $answerArray = [];

        foreach ($answers as $answer) {
            $answerArray[] = $answer->getAnswer();
        }

        return $answerArray;
    }

    /**
     * Get users who have filled test
     *
     * @param int
     * @return array
     */
    public function getUsersForTest($test) {
        $ua = UserAnswer::getByQuestionnaire($test);

        $uids = [];

        foreach ($ua as $an) {
            $uids[] = intval($an->getUserId());
        }

        return array_values(array_unique($uids));
    }
}