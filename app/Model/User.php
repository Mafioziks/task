<?php

namespace Model;

use Model\Model;
use Model\UserAnswer;

class User extends Model {

    /**
     * User id
     */
    private $user_id;

    /**
     * User name
     */
    private $user_name;

    /**
     * Set user id
     *
     * @param int
     */
    public function setId($id) {
        $this->user_id = $id;
        return $this;
    }

    /**
     * Set user name
     *
     * @param string
     */
    public function setName($name) {
        $this->user_name = $name;
        return $this;
    }

    /**
     * Get user id
     *
     * @return int
     */
    public function getId() {
        return $this->user_id;
    }

    /**
     * Get user name
     *
     * @return string
     */
    public function getName() {
        return $this->user_name;
    }

    /**
     * Add user
     */
    public function save() {
        $query = $this->db->prepare('INSERT INTO user (user_name) VALUES (:name)');
        $query->bindValue(':name', $this->getName(), \PDO::PARAM_STR);
        $query->execute();
        $this->setId($this->db->lastInsertId());
    }

    /**
     * Get user answers
     *
     * @return array
     */
    public function getUserAnswers() {
        $query = $this->db->prepare('SELECT * FROM user_answer WHERE user_id = :id');
        $query->bindValue(':id', $this->getId(), \PDO::PARAM_INT);
        $query->execute();

        $userAnswers = [];

        while ($a = $query->fetchObject(UserAnswer::class)) {
            $userAnswers[] = $a;
        }

        return $a;
    }

    /**
     * Get ids for tests user has filled
     *
     * @return array
     */
    public function getFilledTestIds() {
        $query = $this->db->prepare('SELECT questionnaire_id FROM user_answer WHERE user_id = :id');
        $query->bindValue(':id', $this->getId(), \PDO::PARAM_INT);
        $query->execute();

        $questionnaireIds = [];

        $result = $query->fetchAll(\PDO::FETCH_ASSOC);

        foreach ($result as $row) {
            $questionnaireIds[] = intval($row['questionnaire_id']);
        }

        return $questionnaireIds;
    }
}