<?php

namespace Model;

use Model\Model;

class Answer extends Model {

    /**
     * Id of answer
     */
    private $answer_id;
    /**
     * Id of question
     */
    private $question_id;

    /**
     * Content of question
     */
    private $answer;

    /**
     * Question order number in questionnaire
     */
    private $answer_no;

    /**
     * Set id
     *
     * @param int
     */
    public function setId($id) {
        $this->answer_id = $id;
        return $this;
    }

    /**
     * Set question text
     *
     * @param string
     */
    public function setAnswer($answer) {
        $this->answer = $answer;
        return $this;
    }

    /**
     * Set question number of order
     *
     * @param int
     */
    public function setAnswerNo($no) {
        $this->answer_no = $no;
        return $this;
    }

    /**
     * Set questionnaire no
     *
     * @param string
     */
    public function setQuestionId($id) {
        $this->question_id = $id;
        return $this;
    }

    /**
     * Get id
     *
     * @param int
     */
    public function getId() {
        return $this->answer_id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getAnswer() {
        return $this->answer;
    }

    /**
     * Get question number of order
     *
     * @return int
     */
    public function getAnswerNo($no) {
        return $this->answer_no;
    }

    /**
     * Get questionnaire no
     *
     * @return string
     */
    public function getQuestionId($id) {
        return $this->question_id;
    }

    /**
     * Get answer by number and qestion id
     *
     * @param int
     * @param int
     * @return null | \Model\UserAnswer
     */
    public static function getByNoForQuestion($question, $no) {
        $db = Model::connect();
        $query = $db->prepare('SELECT * FROM answer WHERE question_id = :id AND answer_no = :no');
        $query->bindValue(':id', $id, \PDO::PARAM_INT);
        $query->bindValue(':no', $answer, \PDO::PARAM_INT);
        $query->execute();
        if ($query->rowCount() <= 0) {
            return null;
        }

        return $query->fetchObject(__CLASS__);
    }
}