<?php

namespace Model;

class Model {
    protected $db;
    private static $host = 'localhost';
    private static $user = '';
    private static $pass = '';
    private static $database = 'task';
    public static $tableName;

    function __construct() {
        self::$tableName = strtolower(substr(get_called_class(), strrpos(get_called_class(), '\\') + 1, strlen(get_called_class())));
        try {
            $this->db = new \PDO('mysql:host=' . self::$host . ';dbname=' . self::$database . ';charset=utf8', self::$user, self::$pass);
        } catch (\PDOEception $e) {
            echo "Connection failed";
        }
    }

    /**
     * Connect to database statically
     *
     * @return mixed
     */
    public static function connect() {
        try {
            $db = new \PDO('mysql:host=' . self::$host . ';dbname=' . self::$database . ';charset=utf8', self::$user, self::$pass);
            return $db;
        } catch (\PDOEception $e) {
            echo "Connection failed";
        }
        return null;
    }
}
