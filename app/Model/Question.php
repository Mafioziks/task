<?php

namespace Model;

use Model\Model;
use Model\Answer;

class Question extends Model {

    /**
     * Id of question
     */
    private $question_id;

    /**
     * Content of question
     */
    private $question;

    /**
     * Question order number in questionnaire
     */
    private $question_no;

    /**
     * Questionnaire id
     */
    private $questionnaire_id;

    /**
     * Set id
     *
     * @param int
     */
    public function setId($id) {
        $this->questionnaire_id = $id;
        return $this;
    }

    /**
     * Set question text
     *
     * @param string
     */
    public function setQuestion($question) {
        $this->question = $question;
        return $this;
    }

    /**
     * Set question number of order
     *
     * @param int
     */
    public function setQuestionNo($no) {
        $this->question_no = $no;
        return $this;
    }

    /**
     * Set questionnaire no
     *
     * @param string
     */
    public function setQuestionnaireId($id) {
        $this->questionnaire_id = $id;
        return $this;
    }

    /**
     * Get id
     *
     * @param int
     */
    public function getId() {
        return $this->question_id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getQuestion() {
        return $this->question;
    }

    /**
     * Get question number of order
     *
     * @return int
     */
    public function getQuestionNo() {
        return $this->question_no;
    }

    /**
     * Get questionnaire no
     *
     * @return string
     */
    public function getQuestionnaireId() {
        return $this->questionnaire_id;
    }

    /**
     * Get list of questions for questionnaire
     * @param int
     * @return null | array
     */
    public function getQuestionsForQuestionnaire($questionnaire) {
        $query = $this->db->prepare('SELECT * FROM question WHERE questionnaire_id = :id');
        $query->bindValue(':id', $questionnaire, \PDO::PARAM_INT);
        $query->execute();
        if ($query->rowCount() <= 0) {
            return null;
        }

        $questions = [];

        while($q = $query->fetchObject(__CLASS__)) {
            $questions[] = $q;
        }

        return $questions;
    }

    /**
     * @param int
     * @param int
     * @return null | \Model\Questionaire
     */
    public static function getByNoForQuestionnaire($questionnaire, $no) {
        $db = Model::connect();
        $query = $db->prepare('SELECT * FROM question WHERE questionnaire_id = :id AND question_no = :no');
        $query->bindValue(':id', $id, \PDO::PARAM_INT);
        $query->bindValue(':no', $questionnaire, \PDO::PARAM_INT);
        $query->execute();
        if ($query->rowCount() <= 0) {
            return null;
        }

        return $query->fetchObject(__CLASS__);
    }

    /**
     * Get answers for question
     *
     * @return null | \Model\Answer
     */
    public function getAnswers() {
        $query = $this->db->prepare(
            'SELECT * FROM answer WHERE question_id = :question ORDER BY answer_no ASC'
        );
        $query->bindValue(':question', $this->getId(), \PDO::PARAM_INT);
        $query->execute();

        if ($query->rowCount() <= 0) {
            return null;
        }

        $answers = [];

        while ($q = $query->fetchObject(Answer::class)) {
            $answers[] = $q;
        }

        return $answers;
     }
}