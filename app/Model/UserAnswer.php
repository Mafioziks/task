<?php

namespace Model;

use Model\Model;
use Model\Answer;
use Model\Questionnaire;
use Model\Question;

class UserAnswer extends Model {

    /**
     * Id of user
     */
    private $user_id;

    /**
     * Id of questionnaire
     */
    private $questionnaire_id;

    /**
     * Id of question
     */
    private $question_id;

    /**
     * Id of answer
     */
    private $answer_id;

    /**
     * Set user id
     *
     * @param int
     */
    public function setUserId($id) {
        $this->user_id = $id;
        return $this;
    }

    /**
     * Set questionnaire id
     *
     * @param int
     */
    public function setQuestionnaireId($id) {
        $this->questionnaire_id = $id;
        return $this;
    }

    /**
     * Set question id
     *
     * @param int
     */
    public function setQuestionId($id) {
        $this->question_id = $id;
        return $this;
    }

    /**
     * Set answer id
     *
     * @param int
     */
    public function setAnswerId($id) {
        $this->answer_id = $id;
        return $this;
    }

    /**
     * Get user id
     *
     * @param int
     */
    public function getUserId() {
        return $this->user_id;
    }

    /**
     * Get questionnaire id
     *
     * @param int
     */
    public function getQuestionnaireId() {
        return $this->questionnaire_id;
    }

    /**
     * Get question id
     *
     * @param int
     */
    public function getQuestionId() {
        return $this->question_id;
    }

    /**
     * Get answer id
     *
     * @param int
     */
    public function getAnswerId() {
        return $this->answer_id;
    }

    /**
     * Get question by questionare
     *
     * @return mixed
     */
    public static function getByQuestionnaire($questionnaire) {
        $db = parent::connect();
        $query = $db->prepare('SELECT * FROM user_answer WHERE questionnaire_id = :id');
        $query->bindValue(':id', $questionnaire, \PDO::PARAM_INT);
        $query->execute();

        $uAnswers = [];

        while ($a = $query->fetchObject(UserAnswer::class)) {
            $uAnswers[] = $a;
        } 

        return $uAnswers;
    }

     /**
      * Save user answer
      */
     public function save() {
         $query = $this->db->prepare('INSERT INTO user_answer (user_id, questionnaire_id, question_id, answer_id) VALUES (:uid, :questid, :qid, :aid)');
         $query->bindValue(':uid', $this->getUserId(), \PDO::PARAM_INT);
         $query->bindValue(':questid', $this->getQuestionnaireId(), \PDO::PARAM_INT);
         $query->bindValue(':qid', $this->getQuestionId(), \PDO::PARAM_INT);
         $query->bindValue(':aid', $this->getAnswerId(), \PDO::PARAM_INT);
         $query->execute();
     }
}