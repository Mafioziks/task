<?php

namespace Model;

use Model\Model;
use Model\Question;

class Questionnaire extends Model {

    /**
     * Id of questionnaire
     */
    private $questionnaire_id;

    /**
     * Name of questionaire
     */
    private $name;

    /**
     * Set id
     *
     * @param int
     */
    public function setId($id) {
        $this->questionnaire_id = $id;
    }

    /**
     * Set name
     *
     * @param string
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * Get id
     *
     * @param int
     */
    public function getId() {
        return $this->questionnaire_id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Get list of questionnaires
     * 
     * @return null | array
     */
    public function getAllQuestionnaires() {
        $query = $this->db->prepare('SELECT * FROM questionnaire');
        $query->execute();
        if ($query->rowCount() <= 0) {
            return null;
        }

        $questionnaires = [];

        while($q = $query->fetchObject(__CLASS__)) {
            $questionnaires[] = $q;
        }

        return $questionnaires;
    }

    /**
     * Get questionaire by id
     *
     * @return null | \Model\Questionaire
     */
    public static function getById($id) {
        $db = Model::connect();
        $query = $db->prepare('SELECT * FROM questionnaire WHERE questionnaire_id = :id');
        $query->bindValue(':id', $id, \PDO::PARAM_INT);
        $query->execute();
        if ($query->rowCount() <= 0) {
            return null;
        }

        return $query->fetchObject(__CLASS__);
    }

    /**
     * Get questions for questionnaire
     *
     * @return null | \Model\Question
     */
    public function getQuestions() {
        $query = $this->db->prepare(
            'SELECT * FROM question WHERE questionnaire_id = :questionnaire ORDER BY question_no ASC'
        );
        $query->bindValue(':questionnaire', $this->getId(), \PDO::PARAM_INT);
        $query->execute();

        if ($query->rowCount() <= 0) {
            return null;
        }

        $questions = [];

        while ($q = $query->fetchObject(Question::class)) {
            $questions[] = $q;
        }

        return $questions;
     }
}