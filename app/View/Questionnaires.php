<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Testi</title>
        <link type="text/css" rel="stylesheet" href="src/css/default.css" />
    </head>

    <body>
        <div class="content">
            <img id="logo" src="src/img/logo.png"/>
            <div>
                <h3>Pierakstīties</h3>
                <form action="/" method="get">
                    <div class="centerer">
                        <div class="login-form">
                            <label for="uname">Lietotāja vārds:</label>
                            <input id="uname" type="text" name="username" />
                        </div>
                        <button class="btn btn-submit">Nākošais jautājums</button>
                    </div>
                </form>
            </div>
        </div>
        <script type="text/javascript" src="src/js/jquery-3.2.0.slim.min.js"></script>
        <script type="text/javascript" src="src/js/default.js"></script>
    </body>
</html>