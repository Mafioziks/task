<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Testi</title>
        <link type="text/css" rel="stylesheet" href="src/css/default.css" />
    </head>

    <body>
        <div class="content">
            <img id="logo" src="src/img/logo.png"/>
            <div>
                <h3>Testu saraksts</h3>
                <?php foreach ($questionnaires as $questionnaire): ?>
                    <a href="/?questionnaire_id=<?= $questionnaire->getId() ?>"><?= $questionnaire->getName() ?></a>
                <?php endforeach; ?>
            </div>
        </div>
        <script type="text/javascript" src="src/js/jquery-3.2.0.slim.min.js"></script>
        <script type="text/javascript" src="src/js/default.js"></script>
    </body>
</html>