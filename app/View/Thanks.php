<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Testi</title>
        <link type="text/css" rel="stylesheet" href="src/css/default.css" />
    </head>

    <body>
        <div class="content">
            <img id="logo" src="src/img/logo.png"/>
            <div>
                <h3>Tests Pabeigts</h3>
                <form action="/?reset=true" method="post">
                    <div class="centerer">
                        <div class="login-form">
                            <p>Paldies!</p>
                        </div>
                        <button class="btn btn-submit">Uz sākumu</button>
                    </div>
                </form>
            </div>
        </div>
        <script type="text/javascript" src="src/js/jquery-3.2.0.slim.min.js"></script>
        <script type="text/javascript" src="src/js/default.js"></script>
    </body>
</html>