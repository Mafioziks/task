<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Testi</title>
        <link type="text/css" rel="stylesheet" href="src/css/default.css" />
    </head>

    <body>
        <div class="content">
            <img id="logo" src="src/img/logo.png"/>
            <div>
                <h3><?= $question->getQuestion() ?></h3>
                <form action="/?questionnaire_id=<?= $_GET['questionnaire_id'] ?>&question_id=<?= empty($_GET['question_id']) ? 2 :  $_GET['question_id'] + 1 ?>" method="post">
                    <div id="answer-field">
                        <?php foreach ($answers as $a): ?>
                        <div class="btn btn-toggle">
                            <input id="toggable_<?= $a->getId() ?>" class="btn-toggable" type="radio" name="answer" value="<?= $a->getId() ?>"/>
                            <label for="toggable_<?= $a->getId() ?>"><?= $a->getAnswer() ?></label>
                        </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="centerer">
                        <button class="btn btn-submit">Nākošais jautājums</button>
                    </div>
                </form>
            </div>
        </div>
        <script type="text/javascript" src="src/js/jquery-3.2.0.slim.min.js"></script>
        <script type="text/javascript" src="src/js/default.js"></script>
    </body>
</html>